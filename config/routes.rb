Rails.application.routes.draw do
  devise_for :users, controllers: { sessions: 'users/sessions', omniauth_callbacks: 'omniauth_callbacks', registrations: "registrations" }
  root 'pages#home'
  get  'pages/home'
  get  'privacypolicy' => 'pages#privacypolicy'
  get  'cancelpolicy' => 'pages#cancelpolicy'
  get  'about' => 'pages#about'
  get  'join' => 'pages#join'
  get  'my_cancelpolicy' => 'pages#my_cancelpolicy'

  resources :users,     only:     [:show, :index]
  resources :services
  resources :openings do
    resources :exceptions, module: :openings
  end
  resources :conversations, only: [:index, :show, :destroy] do
    member do
      post :reply
    end
  end
  resources :messages, only:      [:new, :create]

  resources :services do
    resources :bookings, only:    [:new, :create]
  end

  resources :bookings, only:      [:edit, :update, :destroy]

  resources :services do
    resources :reviews, only:     [:create, :destroy]
  end

  get 'your_sessions' => 'bookings#your_sessions'
  get 'your_bookings' => 'bookings#your_bookings'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

end
