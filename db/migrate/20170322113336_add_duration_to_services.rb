class AddDurationToServices < ActiveRecord::Migration[5.0]
  def change
    add_column :services, :duration, :integer
  end
end
