class CreateBookings < ActiveRecord::Migration[5.0]
  def change
    create_table :bookings do |t|
      t.references :user, foreign_key: true
      t.references :service, foreign_key: true
      t.datetime :start
      t.datetime :end
      t.integer :price

      t.timestamps
    end
  end
end
