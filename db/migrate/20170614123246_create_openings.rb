class CreateOpenings < ActiveRecord::Migration[5.0]
  def change
    create_table :openings do |t|
      t.datetime :start_time
      t.text :recurring
      t.datetime :end_time
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
