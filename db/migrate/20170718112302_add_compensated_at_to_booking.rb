class AddCompensatedAtToBooking < ActiveRecord::Migration[5.0]
  def change
    add_column :bookings, :compensated_at, :datetime
  end
end
