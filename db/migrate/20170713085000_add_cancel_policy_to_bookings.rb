class AddCancelPolicyToBookings < ActiveRecord::Migration[5.0]
  def change
    add_column :bookings, :cancel_policy, :string
  end
end
