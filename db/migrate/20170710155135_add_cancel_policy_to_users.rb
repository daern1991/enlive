class AddCancelPolicyToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :cancel_policy, :string, :default => "Flexible"
  end
end
