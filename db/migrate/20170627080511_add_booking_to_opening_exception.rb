class AddBookingToOpeningException < ActiveRecord::Migration[5.0]
  def change
    add_reference :opening_exceptions, :booking, foreign_key: true
  end
end
