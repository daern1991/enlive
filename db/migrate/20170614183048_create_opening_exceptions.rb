class CreateOpeningExceptions < ActiveRecord::Migration[5.0]
  def change
    create_table :opening_exceptions do |t|
      t.references :opening, foreign_key: true
      t.datetime :time

      t.timestamps
    end
  end
end
