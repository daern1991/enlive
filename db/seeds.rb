# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

User.create!(first_name:  "Daniel",
             last_name: "Eriksson",
             email: "daniel@example.com",
             password:              "abc123",
             password_confirmation: "abc123",
             description:     "This is the best account. You can buy anything from me and youll love it. Totally. Its great",
             uid: 'asdfasdf',
             avatar: Rails.root.join("app/assets/images/test/daniel.jpg").open,
             confirmed_at: Time.zone.now)

Location.create!(user_id: "1",
                 city: "Stockholm",
                 state: "",
                 country: "Sweden")


User.create!(first_name:  "Lisa-Sanna",
            last_name: "Bjorkebaum Efraimsdotter",
            email: "sanna@example.com",
            password:              "abc123",
            password_confirmation: "abc123",
            description:     "Sanna has extensive personal experience as a body therapist. In just 4 years, she has worked with over 500 clients totalling more than 2000 hours and has held workshops, courses and talks in Chile, Gothenburg, Åre, Östersund, New York, Los Angeles and Stockholm.",
            publishable_key: "pk_test_GLYY4DzvJQp7qfPCl9zujSQ7",
            provider: "stripe_connect",
            uid: "acct_1AJ0Y5I5CWrxHJKG",
            access_code: "sk_test_ES0GTOKMKPQcaKHOH3syDAR3",
            avatar: Rails.root.join("app/assets/images/test/sanna.jpg").open,
            confirmed_at: Time.zone.now)

Location.create!(user_id: "2",
                  city: "New York City",
                  state: "NY",
                  country: "USA")

User.create!(first_name:  "Martina",
            last_name: "Cederqvist",
            email: "martina@example.com",
            password:              "abc123",
            password_confirmation: "abc123",
            description:     "I have always been interested in the body and movement. I started taking dance lessons at age three and continued to dance intensively for 15 years at Visby Dance School including ballet, jazz, hip hop, tap dance and musical / show. As an adult, I studied music for several years, including in the United States where I discovered yoga, which was a natural transition from the dance, which stood very close to me, but that also was intense and tore the body. Yoga became my way of bringing the body and movement with my spiritual interest. Yoga itself has also led me to many courses in personal development and spirituality. In Sweden, I studied health and wellness, trained as a massage therapist and also studied journalism.",
            publishable_key: "pk_test_GLYY4DzvJQp7qfPCl9zujSQ7",
            provider: "stripe_connect",
            uid: "acct_1AJ0Y5I5CWrxHJKG",
            access_code: "sk_test_ES0GTOKMKPQcaKHOH3syDAR3",
            avatar: Rails.root.join("app/assets/images/test/martina.jpg").open,
            confirmed_at: Time.zone.now,)

Location.create!(user_id: "3",
                  city: "Los Angeles",
                  state: "CA",
                  country: "USA")

User.create!(first_name:  "Louise",
            last_name: "Borjesson",
            email: "louise@example.com",
            password:              "abc123",
            password_confirmation: "abc123",
            description:     "",
            publishable_key: "pk_test_GLYY4DzvJQp7qfPCl9zujSQ7",
            provider: "stripe_connect",
            uid: "acct_1AJ0Y5I5CWrxHJKG",
            access_code: "sk_test_ES0GTOKMKPQcaKHOH3syDAR3",
            avatar: Rails.root.join("app/assets/images/test/louise.jpg").open,
            confirmed_at: Time.zone.now)

Location.create!(user_id: "4",
                  city: "Amsterdam",
                  state: "",
                  country: "Holland")

User.create!(first_name:  "John",
            last_name: "Applegate Steiner",
            email: "John@example.com",
            password:              "abc123",
            password_confirmation: "abc123",
            description:     "",
            publishable_key: "pk_test_GLYY4DzvJQp7qfPCl9zujSQ7",
            provider: "stripe_connect",
            uid: "acct_1AJ0Y5I5CWrxHJKG",
            access_code: "sk_test_ES0GTOKMKPQcaKHOH3syDAR3",
            confirmed_at: Time.zone.now)

Location.create!(user_id: "5",
                city: "New York City",
                state: "",
                country: "USA")

User.create!(first_name:  "Timothy",
            last_name: "Applebee Norskbagge",
            email: "tim@example.com",
            password:              "abc123",
            password_confirmation: "abc123",
            description:     "",
            publishable_key: "pk_test_GLYY4DzvJQp7qfPCl9zujSQ7",
            provider: "stripe_connect",
            uid: "acct_1AJ0Y5I5CWrxHJKG",
            access_code: "sk_test_ES0GTOKMKPQcaKHOH3syDAR3",
            confirmed_at: Time.zone.now)

Location.create!(user_id: "6",
                city: "San Francisco",
                state: "",
                country: "USA")

Category.create!(name:  "Bodywork")
Category.create!(name:  "Shamanism")
Category.create!(name:  "Healing")
Category.create!(name:  "Teaching")

Opening.create!(start_time: "2017-07-02 18:00:00", recurring: {:validations=>{}, :rule_type=>"IceCube::DailyRule", :interval=>1}, end_time: nil, user_id: 1)
Opening.create!(start_time: "2017-07-02 18:00:00", recurring: {:validations=>{}, :rule_type=>"IceCube::DailyRule", :interval=>1}, end_time: nil, user_id: 2)
Opening.create!(start_time: "2017-07-02 18:00:00", recurring: {:validations=>{}, :rule_type=>"IceCube::DailyRule", :interval=>1}, end_time: nil, user_id: 3)
Opening.create!(start_time: "2017-07-02 18:00:00", recurring: {:validations=>{}, :rule_type=>"IceCube::DailyRule", :interval=>1}, end_time: nil, user_id: 4)
Opening.create!(start_time: "2017-07-02 18:00:00", recurring: {:validations=>{}, :rule_type=>"IceCube::DailyRule", :interval=>1}, end_time: nil, user_id: 5)
Opening.create!(start_time: "2017-07-02 18:00:00", recurring: {:validations=>{}, :rule_type=>"IceCube::DailyRule", :interval=>1}, end_time: nil, user_id: 6)


Service.create!(title:  "Bodywork 2000",
                user_id: "1",
                price: "100",
                content:     "This is completely friggin amazing. You will love it.",
                duration: 90,
                category_id: 1)

Service.create!(title:  "De-armoring",
                user_id: "1",
                price: "100",
                content:     "This is insanly good. Just for you my friend. Really.",
                duration: 120,
                category_id: 2)

Service.create!(title:  "Shamanism 2000",
                user_id: "1",
                price: "100",
                content:     "This service will help you releasing old energies that no longer serves you.",
                duration: 90,
                category_id: 2)

Service.create!(title:  "Guided Healing",
                user_id: "2",
                price: "100",
                content:     "Through deep relaxation the therapist guides you to feel the blockages in your energy and physical body. The therapist guides you and asks questions to support your awareness and deeper connection to your power and deeper truth. We look into the energy blockages that, for example, can be unsolved conflicts with yourself or others. Sometimes emotions arise during the process and space is given to feel, express and heal.",
                duration: 120,
                category_id: 3)

Service.create!(title:  "Intuitive Massage – For Him",
                user_id: "2",
                price: "100",
                content:     "The therapist supports you in both ways to reconnect with your body and deeper truth. Coaching, meditation, breathwork, a present loving touch, deeper de-armouring/triggerpoint massage, support and space is given to experience, express, heal and listen to insights that often come in that opening. Sanna inspires you to a tantric sexuality without peak-orgasms. Instead discover the opportunities to more intimate relationships, enjoy fullbody-orgasms, to awaken your kundalini-energy and use the sexual energy in all areas of your life.",
                duration: 120,
                category_id: 1)

Service.create!(title:  "Intuitive Massage - For Her",
                user_id: "2",
                price: "100",
                content:     "In a unbalanced world, many women have lost their inner woman. This is an opportunity for you to re-connect with her, practicing letting go, trust, own your sexuality, stand in your power, enjoy being a woman, having a body and emotions. Sometimes we need to be gentle with ourselves and sometimes we need a little push or an outside reflection. Sanna inspires you to a tantric sexuality without peak-orgasms. Instead discover the opportunities to more intimate relationships, enjoy fullbody-orgasms, to awaken your kundalini-energy and use the sexual energy in all areas of your life.",
                duration: 120,
                category_id: 1)

Service.create!(title:  "Couple Session",
                user_id: "2",
                price: "100",
                content:     "Through coaching, exercises and intuitive massage, Sanna will show you new ways and possibilities to a passionate, loving relationship. Sometimes we need to be gentle with ourselves and sometimes we need a little push or an outside reflection. The therapist supports you in both ways to reconnect with your bodies and deeper truths. Sanna inspires you to a tantric sexuality without peak-orgasms. Instead discover the opportunities to more intimate relationships, enjoy fullbody-orgasms, to awaken your kundalini-energy and use the sexual energy in all areas of your life.",
                duration: 120,
                category_id: 4)

Service.create!(title:  "Vaginal De-armoring",
                user_id: "2",
                price: "100",
                content:     "This is an opportunity for you to re-connect with and get to know the woman inside of you, to practice to trust, to surrender, to own your sexuality, to stand in your power, enjoy being a woman, having a body and emotions. Extra focus will be put to re-connect you with your womb, vagina and your natural possibility to feel pleasure and being sensitive in your whole body. A possibility for you to practice how to put healthy boundaries and open up for more love and deeper sex in your life.",
                duration: 120,
                category_id: 2)

Service.create!(title:  "Vaginal De-armoring",
                user_id: "2",
                price: "100",
                content:     "This is an opportunity for you to re-connect with and get to know the woman inside of you, to practice to trust, to surrender, to own your sexuality, to stand in your power, enjoy being a woman, having a body and emotions. Extra focus will be put to re-connect you with your womb, vagina and your natural possibility to feel pleasure and being sensitive in your whole body. A possibility for you to practice how to put healthy boundaries and open up for more love and deeper sex in your life.",
                duration: 120,
                category_id: 2)

Service.create!(title:  "Intuitive Massage for Women",
                user_id: "3",
                price: "100",
                content:     "Do you sometimes feel that you go on the massage and the masseur is a mechanical routine? This massage is one more kind of sympathetic treatment there all you get to take place. With the help of massage, intuitive touch, breathing, yoga and other opening exercises, I am guiding you to more relaxation, harmony and strength in yourself. You also get your exercises to get into a routine of everyday life. The treatment takes inspiration from including classic massage, relaxation massage, pranayama, restorative yoga, Osho meditations and external dearmoring.",
                duration: 120,
                category_id: 1)

Service.create!(title:  "Vaginal De-armoring",
                user_id: "4",
                price: "100",
                content:     'Vaginal and bodily acupressure. Let go of your sexual and physical contractions through a method called "dearmouring". Book a two hour session and initiate a process of a higher bodily awareness. There are many questions before a session - The answers I always send in private mail or through phone.',
                duration: 120,
                category_id: 2)

Service.create!(title:  "Intuitive Massage",
                user_id: "4",
                price: "100",
                content:     "This method is called Intuitive massage and is based on the fact that every client has his or her own needs, energy, health and problems. Therefore, I create every massage session intuitively from person to person. Every session is different and I mix all elements and massage techniques that I have learnt on my path.",
                duration: 120,
                category_id: 1)

Service.create!(title:  "Bodywork 2000",
                user_id: "5",
                price: "100",
                content:     "This is completely friggin amazing. You will love it.",
                duration: 90,
                category_id: 1)



Service.create!(title:  "Bodywork 2000",
                user_id: "6",
                price: "100",
                content:     "This is completely friggin amazing. You will love it.",
                duration: 90,
                category_id: 1)
