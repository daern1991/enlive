require 'test_helper'

class ServicesControllerTest < ActionDispatch::IntegrationTest

  def setup
    @user = User.create(
      first_name: "Foo",
      last_name: "Bar",
      email: "foo@bar.co",
      password: "asdf1234",
      password_confirmation: "asdf1234",
      confirmed_at: Time.zone.now
    )
  end

  test "should redirect to login page" do
    get new_service_url
    assert_redirected_to new_user_session_url
  end

  test "should login and get services index page, then new form and create a new service" do
    sign_in @user
    get services_url
    assert_response :success
    get new_service_url
    assert_template 'services/new'
    title  = "Bodywork"
    content = "Awesome bodywork"
    price = 2000
    category = "Bodywork"


  end

end
