require 'test_helper'

class PagesControllerTest < ActionDispatch::IntegrationTest
  # include Devise::Test::ControllerHelpers
  include Devise::Test::IntegrationHelpers

  def setup
    @user = User.create( 
      first_name: "Foo",
      last_name: "Bar",
      email: "foo@bar.co",
      password: "asdf1234",
      password_confirmation: "asdf1234",
      confirmed_at: Time.zone.now
    )
  end

  test "should login" do
    sign_in @user
    get root_url
    assert_response :success
  end

  test "should get home" do
    get pages_home_url
    assert_response :success
  end

  test "should get privacy policy" do
    get privacypolicy_url
    assert_response :success
  end

end
