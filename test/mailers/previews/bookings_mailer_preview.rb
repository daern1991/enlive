# Preview all emails at http://localhost:3000/rails/mailers/bookings_mailer
class BookingsMailerPreview < ActionMailer::Preview

  def confirmation_email_preview
    BookingsMailer.confirmation_email(User.first, Booking.first)
  end

  def booked_email_preview
    BookingsMailer.booked_email(User.first.services.first, Booking.first)
  end

end
