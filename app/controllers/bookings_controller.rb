class BookingsController < ApplicationController
  before_action :authenticate_user!
  before_action :user_authorization, only: [:edit, :update, :destroy]

  def new
    @opening = params[:opening_id]
    @service = Service.find(params[:service_id])
    @booking = @service.bookings.new
    @booking.build_opening_exception
   unless params[:start_time] == nil
     @start_time = params[:start_time]
   else
      redirect_to @service.user
   end
  end

  def create
    # Amount in cents. (Might want to move the conversion to model instead - business logic)
    @amount = (booking_params[:price].to_i * 100).to_i
    @payout = (booking_params[:price].to_i * 0.96 * 100).to_i
    @service = Service.find(booking_params[:service_id])

    # Set booking to last booking.
    Booking.last.nil? ? @booking_id = 1 : @booking_id = (Booking.last.id + 1)

    customer = Stripe::Customer.create(
      :email => params[:stripeEmail],
      :source  => params[:stripeToken]
    )

    charge = Stripe::Charge.create(
      :customer    => customer.id,
      :amount      => @amount,
      :description => @service.title,
      :currency    => 'eur',

      ## Separate charges and transfers by using transfer group
      :transfer_group => @booking_id
      ## Old way of sending transation right away.
      # :destination => {
      #   :amount => @payout,
      #   :account => @service.user.uid,
      # }
    )

    @booking = current_user.bookings.create(booking_params.merge(:charge_id => charge.id))
    SendEmailJob.set(wait: 20.seconds).perform_later(current_user, current_user.bookings.last, @service)
    CompensateProviderJob.set(wait_until: @booking.start + 24.hours).perform_later(@booking_id, @payout, @service.user.uid, @booking.id)

    flash[:success] = 'Your booking is complete.'

    redirect_to your_sessions_path

  rescue Stripe::CardError => e
    flash[:error] = e.message
    redirect_to request.referer
  end

  def edit
    @booking = Booking.find(params[:id])
  end

  def destroy
    @booking = Booking.find(params[:id])

    if @booking.refundable?
      refund = Stripe::Refund.create({
        :charge => @booking.charge_id,
        :amount => @booking.refund_amount,
      })
      @booking.destroy
      flash[:success] = "Booking cancelled!"
      redirect_to your_sessions_path
    else
      flash[:danger] = "It's too late to cancel this booking!"
      redirect_to your_sessions_path
    end

  end

  def your_sessions
    @sessions = current_user.bookings.upcoming
  end

  def your_bookings
    @services = current_user.services
  end


  private
    def booking_params
      params.require(:booking).permit(:start, :end, :price, :total, :service_id, :cancel_policy, :charge_id, :compensated_at, opening_exception_attributes: [:time, :opening_id])
    end

    def user_authorization
     redirect_to(root_url) unless current_user.id == Booking.find(params[:id]).user_id
    end
end
