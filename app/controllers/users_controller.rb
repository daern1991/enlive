class UsersController < ApplicationController

  def show
    @user = User.find(params[:id])

    if @user.services.exists?
      @services = @user.services

      @userList = User.find(params[:id])
      @servicesList = @userList.services

      @openings = @user.openings
      @calendar_openings = @openings.flat_map{ |e| e.calendar_openings(params.fetch(:start_date, Time.zone.now).to_date) }
    end

    @servicecalendar = Service.find(params[:service]) unless params[:service].nil?
  end

  def index
    scope = params[:users]

    scope.nil? ? @users = User.all : @users = User.joins(:services).where("category_id = ?", scope).distinct
  end

  private

    def service_params
      params.require(:service).permit(:content, :title, :price)
    end

end
