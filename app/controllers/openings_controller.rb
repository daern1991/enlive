class OpeningsController < ApplicationController
  before_action :authenticate_user!, except: [:index]
  before_action :user_authorization, except: [:index, :new, :create]

  def index
    @openings = current_user.openings
    @calendar_openings = @openings.flat_map{ |e| e.calendar_openings(params.fetch(:start_date, Time.zone.now).to_date) }
  end

  def show
    @opening = Opening.find(params[:id])
    begin
      @time = Time.parse(params[:time])
    rescue
      @time = @opening.start_time
    end
  end

  def new
    @opening = Opening.new
  end

  def create
    @opening = current_user.openings.build(opening_params)

    if @opening.save
      flash[:success] = "Opening created!"
      redirect_to openings_path
    else
      render root_path
    end
  end

  def edit
    @opening = Opening.find(params[:id])
  end

  def update
    @opening = Opening.find(params[:id])
    if @opening.update_attributes(opening_params)
      flash[:success] = "Opening updated!"
      redirect_to openings_path
     else
       render 'edit'
     end
  end

  def destroy
    Opening.find(params[:id]).destroy
    flash[:success] = "Opening deleted"
    redirect_to openings_path
  end

    private

      def opening_params
        params.require(:opening).permit(:start_time, :recurring, :end_time, :user_id)
      end

      def user_authorization
       redirect_to(root_url) unless current_user.id == Opening.find(params[:id]).user_id
      end
  end
