class RegistrationsController < Devise::RegistrationsController

  def new
    build_resource({})
    self.resource.location = Location.new
    respond_with self.resource
  end

  def create
    super
  end

  def update
    self.resource = resource_class.to_adapter.get!(send(:"current_#{resource_name}").to_key)
    prev_unconfirmed_email = resource.unconfirmed_email if resource.respond_to?(:unconfirmed_email)

    resource_updated = update_resource(resource, account_update_params)
    yield resource if block_given?
    if resource_updated
      if is_flashing_format?
        flash_key = update_needs_confirmation?(resource, prev_unconfirmed_email) ?
          :update_needs_confirmation : :updated
        set_flash_message :notice, flash_key
      end
      bypass_sign_in resource, scope: resource_name
      respond_with resource, location: after_update_path_for(resource)
    else
      clean_up_passwords resource
      set_minimum_password_length
      if request.referer == services_url
        render '/services/index'
      elsif request.referer == my_cancelpolicy_url
        render '/pages/my_cancelpolicy'
      else
        super
      end
  end
end

  private

  def after_update_path_for(resource)
    request.referer == services_url || my_cancelpolicy_url ? services_path : super
  end

  def after_sign_up_path_for(resource)
    signed_in_root_path(resource)
  end

end
