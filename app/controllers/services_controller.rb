class ServicesController < ApplicationController
  before_action :authenticate_user!, except: [:show, :index]
  before_action :user_authorization, except: [:show, :index, :new, :create]

  # Set_service before any other call in edit, update and destroy.
  before_action :set_service, only: [:edit, :update, :destroy]

  def index
    @services = current_user.services
  end

  def show
    @user = User.find(params[:id])
    @services = @user.services
  end

  def new
    @service = Service.new
    @categories = Category.all.map{|c| [ c.name, c.id ] }
  end

  def create
    @service = current_user.services.build(service_params)
    @service.price = params[:service][:price].gsub(/\D/, '').to_i
    @service.category_id = params[:category_id]

    if @service.save
      flash[:success] = "Service created!"
      redirect_to services_path
    else
      @categories = Category.all.map{|c| [ c.name, c.id ] }
      render :new
    end
  end

  def edit
    @categories = Category.all.map{|c| [ c.name, c.id ] }
  end

  def update
    if @service.update_attributes(service_params)
      flash[:success] = "Service updated!"
      redirect_to services_path
     else
       @categories = Category.all.map{|c| [ c.name, c.id ] }
       render :edit
     end
    @service.category_id = params[:category_id]
  end

  def destroy
    @service.destroy
    flash[:success] = "Service deleted"
    redirect_to services_path
  end

  private

    def service_params
      params.require(:service).permit(:content, :title, :price, :category_id)
    end

    def user_authorization
     redirect_to(root_url) unless current_user.id == Service.find(params[:id]).user_id
    end

    def set_service
      @service = Service.find(params[:id])
    end
end
