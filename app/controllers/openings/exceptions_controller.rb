class Openings::ExceptionsController < ApplicationController
  before_filter :authenticate_user!, only: [:create, :destroy, :update]
  before_action :set_opening

  def create
    if @exception = @opening.opening_exceptions.create(exception_params)
      flash.notice = "Exception added!"
      redirect_to openings_path
    else
      flash.alert = "Unable to add exception"
      redirect_to @opening
    end
  end

  private

    def set_opening
      @opening = Opening.find(params[:opening_id])
    end

    def exception_params
      params.require(:opening_exception).permit(:time)
    end
end
