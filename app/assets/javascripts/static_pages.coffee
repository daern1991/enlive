# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/


$(document).on 'turbolinks:load', -> if window.location.href.indexOf('/cancelpolicy') > -1
  document.querySelector('.btn-flexible').addEventListener 'click', ->
    document.querySelector('.flexible').classList.add 'active'
    document.querySelector('.moderate').classList.remove 'active'
    document.querySelector('.strict').classList.remove 'active'
    return
  document.querySelector('.btn-moderate').addEventListener 'click', ->
    document.querySelector('.flexible').classList.remove 'active'
    document.querySelector('.moderate').classList.add 'active'
    document.querySelector('.strict').classList.remove 'active'
    return
  document.querySelector('.btn-strict').addEventListener 'click', ->
    document.querySelector('.flexible').classList.remove 'active'
    document.querySelector('.moderate').classList.remove 'active'
    document.querySelector('.strict').classList.add 'active'
    return
  return
