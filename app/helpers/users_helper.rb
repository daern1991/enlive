module UsersHelper

  def service_categories(user)
    categories = ""
    user.services.map(&:category).uniq.each do |service|
      categories << service.name
      categories << ", " unless service == user.services.map(&:category).uniq.last
    end
    categories
  end

end
