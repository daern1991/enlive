module ApplicationHelper

  def page_header(text)
    content_for(:page_header) { text.to_s }
  end

  def avatar_for(user, title = user.first_name)
    image_tag user.avatar.url(user.avatar), title: title, class: 'img-rounded', size: '45x45'
  end

end
