class Booking < ApplicationRecord
  belongs_to :user
  belongs_to :service
  has_one :opening_exception, dependent: :destroy
  accepts_nested_attributes_for :opening_exception

  def price_in_cents
    price*100
  end

  def self.upcoming
    ##where(start: (1.day.ago)..DateTime::Infinity.new)
    where("start >= ?", DateTime.now).order("start")
  end

  def self.is_booked?(service, user)
    where("service_id = ? AND user_id = ?", service.id, user.id).present?
  end

  def refundable?
    self.refund_amount > 0
  end

  def refund_amount
    s = self.start - Time.now
    dhms = [60,60,24].reduce([s]) { |m,o| m.unshift(m.shift.divmod(o)).flatten }

    if self.cancel_policy == 'Flexible'
      dhms[0] < 1 ? 0 : self.price * 100
    elsif self.cancel_policy == 'Moderate'
      if dhms[0] > 5
        self.price * 100
      elsif dhms[0] < 5 && dhms[0] > 1
        self.price / 2 * 100
      else
        0
      end
    elsif self.cancel_policy == 'Strict'
      dhms[0] < 7 ? 0 : self.price / 2 * 100
    end
  end

end
