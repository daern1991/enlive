class OpeningException < ApplicationRecord
  belongs_to :opening
  belongs_to :booking
  validates :time, presence: true
end
