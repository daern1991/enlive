class User < ApplicationRecord
  has_many :services, dependent: :destroy
  has_many :bookings
  has_many :reviews
  has_many :openings, dependent: :destroy
  has_one :location, dependent: :destroy

  accepts_nested_attributes_for :services, :location

  scope :bodyworker,  ->{ joins(:services).where("category_id = ?", 1) }
  scope :shaman,      ->{ joins(:services).where("category_id = ?", 2) }
  scope :healer,      ->{ joins(:services).where("category_id = ?", 3) }
  scope :teacher,     ->{ joins(:services).where("category_id = ?", 4) }


  def service_categories(user)
    categories = ""
    user.services.map(&:category).uniq.each do |service|
      categories << service.name
      categories << ", " unless service == user.services.map(&:category).uniq.last
    end
    categories
  end

  acts_as_messageable
  mount_uploader :avatar, AvatarUploader
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :confirmable,
         :recoverable, :rememberable, :trackable, :validatable, :omniauthable

 # User Avatar Validation
 validates_integrity_of  :avatar
 validates_processing_of :avatar

 CANCEL_POLICY_OPTIONS = [
   'Flexible',
   'Moderate',
   'Strict'
 ]

  def mailboxer_email(object)
    email
  end

  def avg_rating
    avg_rating = []
    services.each do |service|
      service.reviews.each do |review|
        avg_rating.push review.star
      end
    end
    return avg_rating.sum.to_f / avg_rating.size
  end

  def received_reviews
    reviews = []
    services.each do |s|
      reviews.push s.reviews.count
    end
    return reviews.sum
  end

  def active_provider?
    services.present? && uid.present? && openings.present?
  end

  private
    def avatar_size_validation
     errors[:avatar] << "should be less than 500KB" if avatar.size > 0.5.megabytes
    end
end
