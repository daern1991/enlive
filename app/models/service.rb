class Service < ApplicationRecord
  belongs_to :user
  belongs_to :category
  has_many :bookings, dependent: :destroy
  has_many :reviews

  validates :user_id, :title, :category, presence: true
  validates :title, length: { maximum: 50 }
  validates :content, length: {minimum: 10, maximum: 600 }
  validates :price, numericality: { only_integer: true, greater_than_or_equal_to: 0 }

  def average_rating
    reviews.count == 0 ? 0 : reviews.average(:star).round(2)
  end

end
