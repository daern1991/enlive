class Opening < ApplicationRecord
  serialize :recurring, Hash

  belongs_to :user
  has_many :opening_exceptions, dependent: :destroy

  def recurring=(value)
     if value == "null"
       super(nil)
     elsif RecurringSelect.is_valid_rule?(value)
       super(RecurringSelect.dirty_hash_to_rule(value).to_hash)
     else
       super(nil)
     end
   end

   def rule
     IceCube::Rule.from_hash recurring
   end

   def schedule(start)
     schedule = IceCube::Schedule.new(start)
     schedule.add_recurrence_rule(rule)

     opening_exceptions.each do |exception|
       schedule.add_exception_time(exception.time)
     end

     schedule
   end

   def calendar_openings(start)
      if recurring.empty?
        [self]
      else
        #start_date = start.beginning_of_month.beginning_of_week
        end_date = start.end_of_month.end_of_week
        schedule(start_time).occurrences(end_date).map do |date|
            Opening.new(id: id, start_time: date)
        end
      end
   end

end
