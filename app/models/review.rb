class Review < ApplicationRecord
  belongs_to :service
  belongs_to :user

  def self.has_review?(service, user)
    where("service_id = ? AND user_id = ?", service.id, user.id).present?
  end

end
