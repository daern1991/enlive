class SendEmailJob < ApplicationJob
  queue_as :default

  def perform(user, booking, service)
    @user = user
    @booking = booking
    @service = service

    BookingsMailer.confirmation_email(@user, @booking).deliver_later
    BookingsMailer.booked_email(@service, @booking).deliver_later
  end
end
