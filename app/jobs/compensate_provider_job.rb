class CompensateProviderJob < ApplicationJob
  queue_as :default

  def perform(transfer_group, payout, account, booking)
    @booking = Booking.find(booking)
    # Do something later
    transfer = Stripe::Transfer.create({
      :amount => payout,
      :currency => "eur",
      :destination => account,
      :transfer_group => transfer_group,
    })

    @booking.update_attributes(compensated_at: Time.now)
  end
end
