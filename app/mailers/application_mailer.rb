class ApplicationMailer < ActionMailer::Base
  default from: 'info@enlive.me'
  layout 'mailer'
end
