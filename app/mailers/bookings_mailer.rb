class BookingsMailer < ApplicationMailer
  default from:'info@enlive.me'

  def confirmation_email(user, booking)
    @user = user
    @booking = booking
    @service = @booking.service

    mail(to: @user.email, subject: 'Booking Confirmation')
  end

  def booked_email(service, booking)
    @service = service
    @booking = booking
    @user = @service.user

    mail(to: @user.email, subject: "New Booking Confirmed with #{@booking.user.first_name}")
  end

end
